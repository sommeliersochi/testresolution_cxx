// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/CPP_MaytonaTestGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPP_MaytonaTestGameMode() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_ACPP_MaytonaTestGameMode_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_ACPP_MaytonaTestGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
// End Cross Module References
	void ACPP_MaytonaTestGameMode::StaticRegisterNativesACPP_MaytonaTestGameMode()
	{
	}
	UClass* Z_Construct_UClass_ACPP_MaytonaTestGameMode_NoRegister()
	{
		return ACPP_MaytonaTestGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CPP_MaytonaTestGameMode.h" },
		{ "ModuleRelativePath", "CPP_MaytonaTestGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPP_MaytonaTestGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::ClassParams = {
		&ACPP_MaytonaTestGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPP_MaytonaTestGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPP_MaytonaTestGameMode, 1470863212);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<ACPP_MaytonaTestGameMode>()
	{
		return ACPP_MaytonaTestGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPP_MaytonaTestGameMode(Z_Construct_UClass_ACPP_MaytonaTestGameMode, &ACPP_MaytonaTestGameMode::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("ACPP_MaytonaTestGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPP_MaytonaTestGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
