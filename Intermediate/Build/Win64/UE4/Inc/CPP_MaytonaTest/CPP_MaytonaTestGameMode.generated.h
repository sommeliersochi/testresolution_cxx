// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPP_MAYTONATEST_CPP_MaytonaTestGameMode_generated_h
#error "CPP_MaytonaTestGameMode.generated.h already included, missing '#pragma once' in CPP_MaytonaTestGameMode.h"
#endif
#define CPP_MAYTONATEST_CPP_MaytonaTestGameMode_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_RPC_WRAPPERS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_MaytonaTestGameMode(); \
	friend struct Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ACPP_MaytonaTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), CPP_MAYTONATEST_API) \
	DECLARE_SERIALIZER(ACPP_MaytonaTestGameMode)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACPP_MaytonaTestGameMode(); \
	friend struct Z_Construct_UClass_ACPP_MaytonaTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ACPP_MaytonaTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), CPP_MAYTONATEST_API) \
	DECLARE_SERIALIZER(ACPP_MaytonaTestGameMode)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CPP_MAYTONATEST_API ACPP_MaytonaTestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_MaytonaTestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CPP_MAYTONATEST_API, ACPP_MaytonaTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_MaytonaTestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CPP_MAYTONATEST_API ACPP_MaytonaTestGameMode(ACPP_MaytonaTestGameMode&&); \
	CPP_MAYTONATEST_API ACPP_MaytonaTestGameMode(const ACPP_MaytonaTestGameMode&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CPP_MAYTONATEST_API ACPP_MaytonaTestGameMode(ACPP_MaytonaTestGameMode&&); \
	CPP_MAYTONATEST_API ACPP_MaytonaTestGameMode(const ACPP_MaytonaTestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CPP_MAYTONATEST_API, ACPP_MaytonaTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_MaytonaTestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_MaytonaTestGameMode)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_9_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class ACPP_MaytonaTestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
