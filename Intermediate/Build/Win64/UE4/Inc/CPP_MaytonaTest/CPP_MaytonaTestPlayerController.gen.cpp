// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/CPP_MaytonaTestPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCPP_MaytonaTestPlayerController() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_ACPP_MaytonaTestPlayerController_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_ACPP_MaytonaTestPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyPickUpBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ACPP_MaytonaTestPlayerController::execPlayerOverlapCoin)
	{
		P_GET_OBJECT(AMyPickUpBase,Z_Param_OverlapedElement);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlayerOverlapCoin(Z_Param_OverlapedElement,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	void ACPP_MaytonaTestPlayerController::StaticRegisterNativesACPP_MaytonaTestPlayerController()
	{
		UClass* Class = ACPP_MaytonaTestPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "PlayerOverlapCoin", &ACPP_MaytonaTestPlayerController::execPlayerOverlapCoin },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics
	{
		struct CPP_MaytonaTestPlayerController_eventPlayerOverlapCoin_Parms
		{
			AMyPickUpBase* OverlapedElement;
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlapedElement;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::NewProp_OverlapedElement = { "OverlapedElement", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CPP_MaytonaTestPlayerController_eventPlayerOverlapCoin_Parms, OverlapedElement), Z_Construct_UClass_AMyPickUpBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CPP_MaytonaTestPlayerController_eventPlayerOverlapCoin_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::NewProp_OverlapedElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "CPP_MaytonaTestPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACPP_MaytonaTestPlayerController, nullptr, "PlayerOverlapCoin", nullptr, nullptr, sizeof(CPP_MaytonaTestPlayerController_eventPlayerOverlapCoin_Parms), Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACPP_MaytonaTestPlayerController_NoRegister()
	{
		return ACPP_MaytonaTestPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACPP_MaytonaTestPlayerController_PlayerOverlapCoin, "PlayerOverlapCoin" }, // 913542312
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CPP_MaytonaTestPlayerController.h" },
		{ "ModuleRelativePath", "CPP_MaytonaTestPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACPP_MaytonaTestPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::ClassParams = {
		&ACPP_MaytonaTestPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x008002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACPP_MaytonaTestPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACPP_MaytonaTestPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACPP_MaytonaTestPlayerController, 2675401762);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<ACPP_MaytonaTestPlayerController>()
	{
		return ACPP_MaytonaTestPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACPP_MaytonaTestPlayerController(Z_Construct_UClass_ACPP_MaytonaTestPlayerController, &ACPP_MaytonaTestPlayerController::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("ACPP_MaytonaTestPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACPP_MaytonaTestPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
