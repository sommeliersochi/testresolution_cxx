// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPP_MAYTONATEST_MyCoinPickUpBase_generated_h
#error "MyCoinPickUpBase.generated.h already included, missing '#pragma once' in MyCoinPickUpBase.h"
#endif
#define CPP_MAYTONATEST_MyCoinPickUpBase_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_RPC_WRAPPERS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyCoinPickUpBase(); \
	friend struct Z_Construct_UClass_AMyCoinPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyCoinPickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyCoinPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMyCoinPickUpBase(); \
	friend struct Z_Construct_UClass_AMyCoinPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyCoinPickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyCoinPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCoinPickUpBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCoinPickUpBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCoinPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCoinPickUpBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCoinPickUpBase(AMyCoinPickUpBase&&); \
	NO_API AMyCoinPickUpBase(const AMyCoinPickUpBase&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCoinPickUpBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCoinPickUpBase(AMyCoinPickUpBase&&); \
	NO_API AMyCoinPickUpBase(const AMyCoinPickUpBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCoinPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCoinPickUpBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCoinPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_12_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMyCoinPickUpBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MyCoinPickUpBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
