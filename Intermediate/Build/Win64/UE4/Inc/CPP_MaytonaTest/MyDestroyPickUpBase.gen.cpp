// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/MyDestroyPickUpBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyDestroyPickUpBase() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyDestroyPickUpBase_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyDestroyPickUpBase();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyPickUpBase();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
	ENGINE_API UClass* Z_Construct_UClass_APawn_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AMyDestroyPickUpBase::execNotifyActorBeginOverlap)
	{
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NotifyActorBeginOverlap(Z_Param_OtherActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyDestroyPickUpBase::execGivePickUpTo)
	{
		P_GET_OBJECT(APawn,Z_Param_PlayerPawn);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GivePickUpTo(Z_Param_PlayerPawn);
		P_NATIVE_END;
	}
	void AMyDestroyPickUpBase::StaticRegisterNativesAMyDestroyPickUpBase()
	{
		UClass* Class = AMyDestroyPickUpBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GivePickUpTo", &AMyDestroyPickUpBase::execGivePickUpTo },
			{ "NotifyActorBeginOverlap", &AMyDestroyPickUpBase::execNotifyActorBeginOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics
	{
		struct MyDestroyPickUpBase_eventGivePickUpTo_Parms
		{
			APawn* PlayerPawn;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerPawn;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_PlayerPawn = { "PlayerPawn", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyDestroyPickUpBase_eventGivePickUpTo_Parms, PlayerPawn), Z_Construct_UClass_APawn_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MyDestroyPickUpBase_eventGivePickUpTo_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MyDestroyPickUpBase_eventGivePickUpTo_Parms), &Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_PlayerPawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyDestroyPickUpBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyDestroyPickUpBase, nullptr, "GivePickUpTo", nullptr, nullptr, sizeof(MyDestroyPickUpBase_eventGivePickUpTo_Parms), Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics
	{
		struct MyDestroyPickUpBase_eventNotifyActorBeginOverlap_Parms
		{
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyDestroyPickUpBase_eventNotifyActorBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyDestroyPickUpBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyDestroyPickUpBase, nullptr, "NotifyActorBeginOverlap", nullptr, nullptr, sizeof(MyDestroyPickUpBase_eventNotifyActorBeginOverlap_Parms), Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMyDestroyPickUpBase_NoRegister()
	{
		return AMyDestroyPickUpBase::StaticClass();
	}
	struct Z_Construct_UClass_AMyDestroyPickUpBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_health_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_health;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyDestroyPickUpBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AMyPickUpBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMyDestroyPickUpBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMyDestroyPickUpBase_GivePickUpTo, "GivePickUpTo" }, // 3175343623
		{ &Z_Construct_UFunction_AMyDestroyPickUpBase_NotifyActorBeginOverlap, "NotifyActorBeginOverlap" }, // 2362804747
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDestroyPickUpBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyDestroyPickUpBase.h" },
		{ "ModuleRelativePath", "MyDestroyPickUpBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyDestroyPickUpBase_Statics::NewProp_health_MetaData[] = {
		{ "ModuleRelativePath", "MyDestroyPickUpBase.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AMyDestroyPickUpBase_Statics::NewProp_health = { "health", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyDestroyPickUpBase, health), METADATA_PARAMS(Z_Construct_UClass_AMyDestroyPickUpBase_Statics::NewProp_health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDestroyPickUpBase_Statics::NewProp_health_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyDestroyPickUpBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyDestroyPickUpBase_Statics::NewProp_health,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyDestroyPickUpBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyDestroyPickUpBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyDestroyPickUpBase_Statics::ClassParams = {
		&AMyDestroyPickUpBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMyDestroyPickUpBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyDestroyPickUpBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyDestroyPickUpBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyDestroyPickUpBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyDestroyPickUpBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyDestroyPickUpBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyDestroyPickUpBase, 3371651360);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<AMyDestroyPickUpBase>()
	{
		return AMyDestroyPickUpBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyDestroyPickUpBase(Z_Construct_UClass_AMyDestroyPickUpBase, &AMyDestroyPickUpBase::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("AMyDestroyPickUpBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyDestroyPickUpBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
