// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPP_MAYTONATEST_MyPickUpBase_generated_h
#error "MyPickUpBase.generated.h already included, missing '#pragma once' in MyPickUpBase.h"
#endif
#define CPP_MAYTONATEST_MyPickUpBase_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_RPC_WRAPPERS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyPickUpBase(); \
	friend struct Z_Construct_UClass_AMyPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyPickUpBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMyPickUpBase(); \
	friend struct Z_Construct_UClass_AMyPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyPickUpBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyPickUpBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyPickUpBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPickUpBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPickUpBase(AMyPickUpBase&&); \
	NO_API AMyPickUpBase(const AMyPickUpBase&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPickUpBase(AMyPickUpBase&&); \
	NO_API AMyPickUpBase(const AMyPickUpBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPickUpBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisisionComponent() { return STRUCT_OFFSET(AMyPickUpBase, CollisisionComponent); } \
	FORCEINLINE static uint32 __PPO__RespawnTime() { return STRUCT_OFFSET(AMyPickUpBase, RespawnTime); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_13_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMyPickUpBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MyPickUpBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
