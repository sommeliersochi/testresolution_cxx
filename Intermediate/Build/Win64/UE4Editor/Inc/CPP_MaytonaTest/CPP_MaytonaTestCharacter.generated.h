// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPP_MAYTONATEST_CPP_MaytonaTestCharacter_generated_h
#error "CPP_MaytonaTestCharacter.generated.h already included, missing '#pragma once' in CPP_MaytonaTestCharacter.h"
#endif
#define CPP_MAYTONATEST_CPP_MaytonaTestCharacter_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execGetMoney);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddMoney); \
	DECLARE_FUNCTION(execGetMoney);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACPP_MaytonaTestCharacter(); \
	friend struct Z_Construct_UClass_ACPP_MaytonaTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ACPP_MaytonaTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(ACPP_MaytonaTestCharacter)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACPP_MaytonaTestCharacter(); \
	friend struct Z_Construct_UClass_ACPP_MaytonaTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ACPP_MaytonaTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(ACPP_MaytonaTestCharacter)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACPP_MaytonaTestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACPP_MaytonaTestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_MaytonaTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_MaytonaTestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_MaytonaTestCharacter(ACPP_MaytonaTestCharacter&&); \
	NO_API ACPP_MaytonaTestCharacter(const ACPP_MaytonaTestCharacter&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACPP_MaytonaTestCharacter(ACPP_MaytonaTestCharacter&&); \
	NO_API ACPP_MaytonaTestCharacter(const ACPP_MaytonaTestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACPP_MaytonaTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACPP_MaytonaTestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACPP_MaytonaTestCharacter)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ACPP_MaytonaTestCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ACPP_MaytonaTestCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ACPP_MaytonaTestCharacter, CursorToWorld); } \
	FORCEINLINE static uint32 __PPO__Money() { return STRUCT_OFFSET(ACPP_MaytonaTestCharacter, Money); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_11_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class ACPP_MaytonaTestCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_CPP_MaytonaTestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
