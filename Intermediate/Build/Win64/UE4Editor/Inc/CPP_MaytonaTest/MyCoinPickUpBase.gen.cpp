// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/MyCoinPickUpBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyCoinPickUpBase() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyCoinPickUpBase_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyCoinPickUpBase();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyPickUpBase();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
// End Cross Module References
	void AMyCoinPickUpBase::StaticRegisterNativesAMyCoinPickUpBase()
	{
	}
	UClass* Z_Construct_UClass_AMyCoinPickUpBase_NoRegister()
	{
		return AMyCoinPickUpBase::StaticClass();
	}
	struct Z_Construct_UClass_AMyCoinPickUpBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyCoinPickUpBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AMyPickUpBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyCoinPickUpBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyCoinPickUpBase.h" },
		{ "ModuleRelativePath", "MyCoinPickUpBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyCoinPickUpBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyCoinPickUpBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyCoinPickUpBase_Statics::ClassParams = {
		&AMyCoinPickUpBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyCoinPickUpBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyCoinPickUpBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyCoinPickUpBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyCoinPickUpBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyCoinPickUpBase, 3495999260);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<AMyCoinPickUpBase>()
	{
		return AMyCoinPickUpBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyCoinPickUpBase(Z_Construct_UClass_AMyCoinPickUpBase, &AMyCoinPickUpBase::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("AMyCoinPickUpBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyCoinPickUpBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
