// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class APawn;
#ifdef CPP_MAYTONATEST_MyDestroyPickUpBase_generated_h
#error "MyDestroyPickUpBase.generated.h already included, missing '#pragma once' in MyDestroyPickUpBase.h"
#endif
#define CPP_MAYTONATEST_MyDestroyPickUpBase_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execNotifyActorBeginOverlap); \
	DECLARE_FUNCTION(execGivePickUpTo);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execNotifyActorBeginOverlap); \
	DECLARE_FUNCTION(execGivePickUpTo);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyDestroyPickUpBase(); \
	friend struct Z_Construct_UClass_AMyDestroyPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyDestroyPickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyDestroyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMyDestroyPickUpBase(); \
	friend struct Z_Construct_UClass_AMyDestroyPickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMyDestroyPickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyDestroyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyDestroyPickUpBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyDestroyPickUpBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDestroyPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDestroyPickUpBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDestroyPickUpBase(AMyDestroyPickUpBase&&); \
	NO_API AMyDestroyPickUpBase(const AMyDestroyPickUpBase&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyDestroyPickUpBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDestroyPickUpBase(AMyDestroyPickUpBase&&); \
	NO_API AMyDestroyPickUpBase(const AMyDestroyPickUpBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDestroyPickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDestroyPickUpBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyDestroyPickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__health() { return STRUCT_OFFSET(AMyDestroyPickUpBase, health); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_13_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMyDestroyPickUpBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MyDestroyPickUpBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
