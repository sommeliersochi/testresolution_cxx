// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/MyPickUpBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyPickUpBase() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyPickUpBase_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyPickUpBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
	ENGINE_API UClass* Z_Construct_UClass_USphereComponent_NoRegister();
// End Cross Module References
	void AMyPickUpBase::StaticRegisterNativesAMyPickUpBase()
	{
	}
	UClass* Z_Construct_UClass_AMyPickUpBase_NoRegister()
	{
		return AMyPickUpBase::StaticClass();
	}
	struct Z_Construct_UClass_AMyPickUpBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisisionComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisisionComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RespawnTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RespawnTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyPickUpBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyPickUpBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyPickUpBase.h" },
		{ "ModuleRelativePath", "MyPickUpBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_CollisisionComponent_MetaData[] = {
		{ "Category", "PickUp" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyPickUpBase.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_CollisisionComponent = { "CollisisionComponent", nullptr, (EPropertyFlags)0x00200800000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyPickUpBase, CollisisionComponent), Z_Construct_UClass_USphereComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_CollisisionComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_CollisisionComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_RespawnTime_MetaData[] = {
		{ "Category", "PickUp" },
		{ "ModuleRelativePath", "MyPickUpBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_RespawnTime = { "RespawnTime", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyPickUpBase, RespawnTime), METADATA_PARAMS(Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_RespawnTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_RespawnTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyPickUpBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_CollisisionComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyPickUpBase_Statics::NewProp_RespawnTime,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyPickUpBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyPickUpBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyPickUpBase_Statics::ClassParams = {
		&AMyPickUpBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyPickUpBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyPickUpBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyPickUpBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyPickUpBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyPickUpBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyPickUpBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyPickUpBase, 1766241067);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<AMyPickUpBase>()
	{
		return AMyPickUpBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyPickUpBase(Z_Construct_UClass_AMyPickUpBase, &AMyPickUpBase::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("AMyPickUpBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyPickUpBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
