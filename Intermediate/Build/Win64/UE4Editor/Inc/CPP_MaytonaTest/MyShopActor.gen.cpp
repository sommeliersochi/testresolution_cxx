// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CPP_MaytonaTest/MyShopActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyShopActor() {}
// Cross Module References
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyShopActor_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_AMyShopActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_CPP_MaytonaTest();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	CPP_MAYTONATEST_API UClass* Z_Construct_UClass_UMyInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AMyShopActor::execInitialScaleMesh)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitialScaleMesh();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyShopActor::execNewScaleMesh)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->NewScaleMesh();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AMyShopActor::execInteract)
	{
		P_GET_OBJECT(AActor,Z_Param_Interactor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Interact(Z_Param_Interactor);
		P_NATIVE_END;
	}
	void AMyShopActor::StaticRegisterNativesAMyShopActor()
	{
		UClass* Class = AMyShopActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InitialScaleMesh", &AMyShopActor::execInitialScaleMesh },
			{ "Interact", &AMyShopActor::execInteract },
			{ "NewScaleMesh", &AMyShopActor::execNewScaleMesh },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyShopActor, nullptr, "InitialScaleMesh", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyShopActor_InitialScaleMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyShopActor_InitialScaleMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyShopActor_Interact_Statics
	{
		struct MyShopActor_eventInteract_Parms
		{
			AActor* Interactor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Interactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AMyShopActor_Interact_Statics::NewProp_Interactor = { "Interactor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MyShopActor_eventInteract_Parms, Interactor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMyShopActor_Interact_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMyShopActor_Interact_Statics::NewProp_Interactor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyShopActor_Interact_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyShopActor_Interact_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyShopActor, nullptr, "Interact", nullptr, nullptr, sizeof(MyShopActor_eventInteract_Parms), Z_Construct_UFunction_AMyShopActor_Interact_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyShopActor_Interact_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyShopActor_Interact_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyShopActor_Interact_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyShopActor_Interact()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyShopActor_Interact_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyShopActor, nullptr, "NewScaleMesh", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyShopActor_NewScaleMesh()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyShopActor_NewScaleMesh_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMyShopActor_NoRegister()
	{
		return AMyShopActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyShopActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShopMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShopMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriggerComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TriggerComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShopCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShopCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsScaleActivate_MetaData[];
#endif
		static void NewProp_IsScaleActivate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsScaleActivate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyShopActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_CPP_MaytonaTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMyShopActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMyShopActor_InitialScaleMesh, "InitialScaleMesh" }, // 3107342937
		{ &Z_Construct_UFunction_AMyShopActor_Interact, "Interact" }, // 1583151830
		{ &Z_Construct_UFunction_AMyShopActor_NewScaleMesh, "NewScaleMesh" }, // 4009420874
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyShopActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyShopActor.h" },
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopMesh_MetaData[] = {
		{ "Category", "MyShopActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopMesh = { "ShopMesh", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyShopActor, ShopMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyShopActor_Statics::NewProp_TriggerComponent_MetaData[] = {
		{ "Category", "MyShopActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyShopActor_Statics::NewProp_TriggerComponent = { "TriggerComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyShopActor, TriggerComponent), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyShopActor_Statics::NewProp_TriggerComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::NewProp_TriggerComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopCameraComponent_MetaData[] = {
		{ "Category", "MyShopActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopCameraComponent = { "ShopCameraComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyShopActor, ShopCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate_MetaData[] = {
		{ "ModuleRelativePath", "MyShopActor.h" },
	};
#endif
	void Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate_SetBit(void* Obj)
	{
		((AMyShopActor*)Obj)->IsScaleActivate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate = { "IsScaleActivate", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMyShopActor), &Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyShopActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyShopActor_Statics::NewProp_TriggerComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyShopActor_Statics::NewProp_ShopCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyShopActor_Statics::NewProp_IsScaleActivate,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AMyShopActor_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMyInterface_NoRegister, (int32)VTABLE_OFFSET(AMyShopActor, IMyInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyShopActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyShopActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyShopActor_Statics::ClassParams = {
		&AMyShopActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMyShopActor_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyShopActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyShopActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyShopActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyShopActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyShopActor, 4019103322);
	template<> CPP_MAYTONATEST_API UClass* StaticClass<AMyShopActor>()
	{
		return AMyShopActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyShopActor(Z_Construct_UClass_AMyShopActor, &AMyShopActor::StaticClass, TEXT("/Script/CPP_MaytonaTest"), TEXT("AMyShopActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyShopActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
