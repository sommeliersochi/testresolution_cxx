// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef CPP_MAYTONATEST_MyShopActor_generated_h
#error "MyShopActor.generated.h already included, missing '#pragma once' in MyShopActor.h"
#endif
#define CPP_MAYTONATEST_MyShopActor_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInitialScaleMesh); \
	DECLARE_FUNCTION(execNewScaleMesh); \
	DECLARE_FUNCTION(execInteract);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInitialScaleMesh); \
	DECLARE_FUNCTION(execNewScaleMesh); \
	DECLARE_FUNCTION(execInteract);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyShopActor(); \
	friend struct Z_Construct_UClass_AMyShopActor_Statics; \
public: \
	DECLARE_CLASS(AMyShopActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyShopActor) \
	virtual UObject* _getUObject() const override { return const_cast<AMyShopActor*>(this); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyShopActor(); \
	friend struct Z_Construct_UClass_AMyShopActor_Statics; \
public: \
	DECLARE_CLASS(AMyShopActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMyShopActor) \
	virtual UObject* _getUObject() const override { return const_cast<AMyShopActor*>(this); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyShopActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyShopActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyShopActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyShopActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyShopActor(AMyShopActor&&); \
	NO_API AMyShopActor(const AMyShopActor&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyShopActor(AMyShopActor&&); \
	NO_API AMyShopActor(const AMyShopActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyShopActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyShopActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyShopActor)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_PRIVATE_PROPERTY_OFFSET
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_12_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMyShopActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MyShopActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
