// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
class APawn;
#ifdef CPP_MAYTONATEST_MySkalePickUpBase_generated_h
#error "MySkalePickUpBase.generated.h already included, missing '#pragma once' in MySkalePickUpBase.h"
#endif
#define CPP_MAYTONATEST_MySkalePickUpBase_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execGivePickUpTo); \
	DECLARE_FUNCTION(execInteract);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapBegin); \
	DECLARE_FUNCTION(execGivePickUpTo); \
	DECLARE_FUNCTION(execInteract);


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySkalePickUpBase(); \
	friend struct Z_Construct_UClass_AMySkalePickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMySkalePickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMySkalePickUpBase) \
	virtual UObject* _getUObject() const override { return const_cast<AMySkalePickUpBase*>(this); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMySkalePickUpBase(); \
	friend struct Z_Construct_UClass_AMySkalePickUpBase_Statics; \
public: \
	DECLARE_CLASS(AMySkalePickUpBase, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMySkalePickUpBase) \
	virtual UObject* _getUObject() const override { return const_cast<AMySkalePickUpBase*>(this); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySkalePickUpBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySkalePickUpBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySkalePickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySkalePickUpBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySkalePickUpBase(AMySkalePickUpBase&&); \
	NO_API AMySkalePickUpBase(const AMySkalePickUpBase&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySkalePickUpBase(AMySkalePickUpBase&&); \
	NO_API AMySkalePickUpBase(const AMySkalePickUpBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySkalePickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySkalePickUpBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySkalePickUpBase)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_PRIVATE_PROPERTY_OFFSET
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_14_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMySkalePickUpBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MySkalePickUpBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
