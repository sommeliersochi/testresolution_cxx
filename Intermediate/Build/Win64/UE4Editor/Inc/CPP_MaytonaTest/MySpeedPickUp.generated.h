// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CPP_MAYTONATEST_MySpeedPickUp_generated_h
#error "MySpeedPickUp.generated.h already included, missing '#pragma once' in MySpeedPickUp.h"
#endif
#define CPP_MAYTONATEST_MySpeedPickUp_generated_h

#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_SPARSE_DATA
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_RPC_WRAPPERS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySpeedPickUp(); \
	friend struct Z_Construct_UClass_AMySpeedPickUp_Statics; \
public: \
	DECLARE_CLASS(AMySpeedPickUp, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMySpeedPickUp)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAMySpeedPickUp(); \
	friend struct Z_Construct_UClass_AMySpeedPickUp_Statics; \
public: \
	DECLARE_CLASS(AMySpeedPickUp, AMyPickUpBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CPP_MaytonaTest"), NO_API) \
	DECLARE_SERIALIZER(AMySpeedPickUp)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySpeedPickUp(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySpeedPickUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySpeedPickUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySpeedPickUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySpeedPickUp(AMySpeedPickUp&&); \
	NO_API AMySpeedPickUp(const AMySpeedPickUp&); \
public:


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySpeedPickUp() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySpeedPickUp(AMySpeedPickUp&&); \
	NO_API AMySpeedPickUp(const AMySpeedPickUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySpeedPickUp); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySpeedPickUp); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySpeedPickUp)


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CooldownTime() { return STRUCT_OFFSET(AMySpeedPickUp, CooldownTime); }


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_15_PROLOG
#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_RPC_WRAPPERS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_INCLASS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_PRIVATE_PROPERTY_OFFSET \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_SPARSE_DATA \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_INCLASS_NO_PURE_DECLS \
	CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CPP_MAYTONATEST_API UClass* StaticClass<class AMySpeedPickUp>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CPP_MaytonaTest_Source_CPP_MaytonaTest_MySpeedPickUp_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
