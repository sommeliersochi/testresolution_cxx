// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CPP_MaytonaTest : ModuleRules
{
	public CPP_MaytonaTest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
