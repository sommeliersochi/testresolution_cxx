// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPP_MaytonaTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CPP_MaytonaTest, "CPP_MaytonaTest" );

DEFINE_LOG_CATEGORY(LogCPP_MaytonaTest)
 