// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "MySkalePickUpBase.h"
#include "GameFramework/Character.h"
#include "CPP_MaytonaTestCharacter.generated.h"

UCLASS(Blueprintable)
class ACPP_MaytonaTestCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ACPP_MaytonaTestCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UFUNCTION(BlueprintGetter)
	int GetMoney() const;
	
	UFUNCTION(BlueprintCallable)
	int AddMoney(int AddMoney);

	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	void OverlapScale (AMySkalePickUpBase* CollisisionComponent, AActor* Other);
protected:	
	UPROPERTY(BlueprintReadWrite,Category = Money)
	int Money = 0;
};

