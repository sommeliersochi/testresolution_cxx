// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCoinPickUpBase.h"
#include "CPP_MaytonaTestCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBaseCoin, All, All)
/*void AMyCoinPickUpBase::AddMoney(float Coin)
{
	Coin++;
}*/
bool AMyCoinPickUpBase::GivePickUpTo(APawn* PlayerPawn)
{
	UE_LOG(LogPickUpBaseCoin, Display, TEXT("Coin was Taken"));
	return true;
	
}

void AMyCoinPickUpBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	
	Super::NotifyActorBeginOverlap(OtherActor);
	const auto Pawn = Cast<APawn>(OtherActor);
	
	if (GivePickUpTo(Pawn))
	{
		ACPP_MaytonaTestCharacter* Character = Cast<ACPP_MaytonaTestCharacter>(Pawn);
		Character -> AddMoney(Coin);
	}

}





