// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPickUpBase.h"
#include "MyCoinPickUpBase.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAYTONATEST_API AMyCoinPickUpBase : public AMyPickUpBase

{
	GENERATED_BODY()
	
	public:
	virtual bool GivePickUpTo (APawn* PlayerPawn) override;
	
	protected:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	
	private:
	int Coin = 3;	
};
