// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDestroyPickUpBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBaseDestroy, All, All)


bool AMyDestroyPickUpBase::GivePickUpTo(APawn* PlayerPawn)
{
	UE_LOG(LogPickUpBaseDestroy, Display, TEXT("Destroy was Taken"));
	return true;
}

void AMyDestroyPickUpBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	const auto Pawn = Cast<APawn>(OtherActor);
	if (GivePickUpTo(Pawn))
	{		
		if(health > 0)
		{
			-- health;			
		}
		else Destroy();
	}
	UE_LOG(LogTemp,Warning, TEXT("health %i"),health);
	
}

