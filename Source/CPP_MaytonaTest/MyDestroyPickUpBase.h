// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MyPickUpBase.h"
#include "MyDestroyPickUpBase.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAYTONATEST_API AMyDestroyPickUpBase : public AMyPickUpBase
{
	GENERATED_BODY()

public:	
	UFUNCTION()
	virtual bool GivePickUpTo (APawn* PlayerPawn) override;
	
	UFUNCTION()
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

private:
	UPROPERTY()
	int health = 2;

	

	
};





