// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPickUpBase.h"

#include "CPP_MaytonaTestCharacter.h"
#include "Components/SphereComponent.h"

class ACPP_MaytonaTestCharacter;

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBase, All, All)

// Sets default values
AMyPickUpBase::AMyPickUpBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisisionComponent->InitSphereRadius(30.0f);
	CollisisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisisionComponent);
}

// Called when the game starts or when spawned
void AMyPickUpBase::BeginPlay()
{
	Super::BeginPlay();
	check(CollisisionComponent);
	GeneratonRotation();
}

// Called every frame
void AMyPickUpBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
}

void AMyPickUpBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	auto Character = Cast<ACPP_MaytonaTestCharacter>(OtherActor);
	if (IsValid(Character))
	{
		Super::NotifyActorBeginOverlap(OtherActor);
		const auto Pawn = Cast<APawn>(OtherActor);
		if (GivePickUpTo(Pawn))
		{
			PickUpWasTaken();
		}
	}
}

bool AMyPickUpBase::GivePickUpTo(APawn* PlayerPawn)
{
	return false;
}

void AMyPickUpBase::PickUpWasTaken()
{
	CollisisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(false, true);
	}
	FTimerHandle RespawnTimerHandle;
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AMyPickUpBase::Respawn, RespawnTime);
}

void AMyPickUpBase::Respawn() const
{
	CollisisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(true, true);
	}
}

void AMyPickUpBase::GeneratonRotation()
{
	const auto Direction = FMath::RandBool() ? 1.0f : -1.0f;
	RotationYaw = FMath::RandRange(1.0f, 2.0f) * Direction;
}
