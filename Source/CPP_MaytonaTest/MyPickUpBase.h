// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "MyPickUpBase.generated.h"

class USphereComponent;

UCLASS()
class CPP_MAYTONATEST_API AMyPickUpBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMyPickUpBase();

protected:
	UPROPERTY(VisibleAnywhere, Category="PickUp")
	USphereComponent* CollisisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="PickUp")
	float RespawnTime = 3.0f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	float RotationYaw = 0.0f;
	
	virtual bool GivePickUpTo (APawn* PlayerPawn);

	void PickUpWasTaken();
	void Respawn() const;
	void GeneratonRotation();
	
};
