// Fill out your copyright notice in the Description page of Project Settings.


#include "MyShopActor.h"

#include "Components/StaticMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"


// Sets default values
AMyShopActor::AMyShopActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ShopMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	TriggerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	TriggerComponent -> SetupAttachment(ShopMesh);	
	ShopCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("ShopCamera"));
	ShopCameraComponent -> SetupAttachment(ShopMesh);
	SetRootComponent(ShopMesh);
	

}

// Called when the game starts or when spawned
void AMyShopActor::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AMyShopActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMyShopActor::Interact(AActor* Interactor)
{
}

void AMyShopActor::NewScaleMesh()
{
	ShopMesh->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));
	IsScaleActivate = true;	
}

void AMyShopActor::InitialScaleMesh()
{
	ShopMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
	IsScaleActivate = false;
}

