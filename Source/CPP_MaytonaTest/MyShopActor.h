// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MyInterface.h"
#include "GameFramework/Actor.h"
#include "MyShopActor.generated.h"


UCLASS()
class CPP_MAYTONATEST_API AMyShopActor : public AActor, public IMyInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMyShopActor();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* ShopMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* TriggerComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCameraComponent* ShopCameraComponent;

	UPROPERTY()
	bool IsScaleActivate;
	
	UFUNCTION()
	virtual void Interact (AActor* Interactor) override;
	
	
	UFUNCTION()
	void NewScaleMesh();

	UFUNCTION()
	void InitialScaleMesh();
};
