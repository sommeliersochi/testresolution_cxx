// Fill out your copyright notice in the Description page of Project Settings.


#include "MySkalePickUpBase.h"

#include "CPP_MaytonaTestCharacter.h"
#include "MyShopActor.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBaseSkale, All, All)
AMySkalePickUpBase::AMySkalePickUpBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CollisisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AMySkalePickUpBase::OnOverlapBegin);
}


void AMySkalePickUpBase::Interact(AActor* Interactor)
{
	auto Shop = Cast<AMyShopActor>(Interactor);
	if(IsValid(Shop))
	{
		Shop->NewScaleMesh();
	}
}

bool AMySkalePickUpBase::GivePickUpTo(APawn* PlayerPawn)
{
	UE_LOG(LogPickUpBaseSkale, Display, TEXT("Skale was Taken"));
	return true;
}

inline void AMySkalePickUpBase::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                               UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                               const FHitResult& SweepResult)
{
	auto Character = Cast<ACPP_MaytonaTestCharacter>(OtherActor);
	
	if (IsValid(Character))
	{		
		for (AMyShopActor* Shops : ShopInPlayce)
			if (IsValid(Shops))
			{
				if (Shops->IsScaleActivate)
				{
					Shops->InitialScaleMesh();
					UE_LOG(LogPickUpBaseSkale, Display, TEXT("Scale was Change"));
				}
				else
				{
					Shops->NewScaleMesh();
				}
			}
	}
}




