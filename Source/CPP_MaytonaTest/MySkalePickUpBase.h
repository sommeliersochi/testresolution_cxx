// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MyInterface.h"
#include "MyPickUpBase.h"
#include "MySkalePickUpBase.generated.h"

/**
 * 
 */
UCLASS()
class CPP_MAYTONATEST_API AMySkalePickUpBase : public AMyPickUpBase, public IMyInterface
{
	GENERATED_BODY()

	AMySkalePickUpBase();
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class AMyShopActor*> ShopInPlayce;

	UFUNCTION()
	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
	virtual bool GivePickUpTo(APawn* PlayerPawn) override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                    const FHitResult& SweepResult);


};
