// Fill out your copyright notice in the Description page of Project Settings.


#include "MySpeedPickUp.h"
#include "CPP_MaytonaTestCharacter.h"

#include "GameFramework/CharacterMovementComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUpBaseSpeed, All, All)
DECLARE_DELEGATE(FTimerDelegate);
FTimerDelegate TimerDel;

bool AMySpeedPickUp::GivePickUpTo(APawn* PlayerPawn)
{
	UE_LOG(LogPickUpBaseSpeed, Display, TEXT("Speed was Taken"));
	return true;	
}

void AMySpeedPickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
	auto Character = Cast<ACPP_MaytonaTestCharacter>(OtherActor);
	if(IsValid(Character))
	{
		Super::NotifyActorBeginOverlap(OtherActor);
		//const auto Pawn = Cast<APawn>(OtherActor);
		if (GivePickUpTo(Character))
		{
			//ACPP_MaytonaTestCharacter* Character = Cast<ACPP_MaytonaTestCharacter>(Pawn);
			GetSpeedBoost (Character, 2);
		}
		//ACPP_MaytonaTestCharacter* Character = Cast<ACPP_MaytonaTestCharacter>(Pawn);
		FTimerHandle CoolDownTimerHandle;
		TimerDel.BindUObject(this, &AMySpeedPickUp::GetSpeedNormal, Character, 600.0f );	
		GetWorldTimerManager().SetTimer(CoolDownTimerHandle,TimerDel,30.0f,false);
	}

}

void AMySpeedPickUp::GetSpeedBoost(ACPP_MaytonaTestCharacter* PlayerCharacter,float SpeedBonus)
{
	PlayerCharacter->GetCharacterMovement()->MaxWalkSpeed *= SpeedBonus;
}



void AMySpeedPickUp::GetSpeedNormal(ACPP_MaytonaTestCharacter* PlayerCharacter, const float InitialSpeed)
{
	PlayerCharacter->GetCharacterMovement()->MaxWalkSpeed = InitialSpeed;
	UE_LOG(LogPickUpBaseSpeed, Display, TEXT("SpeedNormal was Taken"));
}
