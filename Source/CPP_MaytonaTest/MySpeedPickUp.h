// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CPP_MaytonaTestCharacter.h"
#include "MyPickUpBase.h"
#include "MySpeedPickUp.generated.h"

/*class UCharacterMovementComponent;*/
/**
 * 
 */
UCLASS()
class CPP_MAYTONATEST_API AMySpeedPickUp : public AMyPickUpBase
{
	GENERATED_BODY()
public:
	//ACPP_MaytonaTestCharacter* Character = Cast<ACPP_MaytonaTestCharacter>(Pawn);
	private:
	virtual bool GivePickUpTo (APawn* PlayerPawn) override;
	/*UCharacterMovementComponent * GetCharacterMovement() const;*/
	protected:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="PickUp")
	float CooldownTime = 30.0f;

	void GetSpeedBoost (ACPP_MaytonaTestCharacter*PlayerCharacter, float SpeedBonus);
	void GetSpeedNormal (ACPP_MaytonaTestCharacter*PlayerCharacter, float InitialSpeed);
	//void Cooldown();
	
};


